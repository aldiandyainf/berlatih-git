<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        require_once('./Animal.php');
        require_once('./Frog.php');
        require_once('./Ape.php');
    ?>

    <?php 
    $sheep = new Animal("shaun");
    ?> 

    <fieldset>
        <legend>Release 0:</legend>
        <?php 
            echo $sheep->name;
            echo "<br>";
            echo $sheep->legs;
            echo "<br>";
            echo $sheep->cold_blooded;
            echo "<br>";
        ?>
    </fieldset>

    <fieldset>
        <legend>Release 1:</legend>
        <?php
            $sungokong = new Ape("kera sakti");
            $sungokong->yell();
            echo "<br>";
            $kodok = new Frog("buduk");
            $kodok->jump() ; //


        ?>
    </fieldset>

    <fieldset>
        <legend>Output Akhir</legend>
        <?php 
            echo "Name : ";
            echo $sheep->name;
            echo "<br>Legs : ";
            echo $sheep->legs;
            echo "<br>Cold Blooded : ";
            echo $sheep->cold_blooded;

            
            echo "<br><br>Name : ";
            echo $kodok->name;
            echo "<br>Legs : ";
            echo $kodok->legs;
            echo "<br>Cold Blooded : ";
            echo $kodok->cold_blooded;
            echo "<br>Jump : ";
            $kodok->jump();

            echo "<br><br>Name : ";
            echo $sungokong->name;
            echo "<br>Legs : ";
            echo $sungokong->legs;
            echo "<br>Cold Blooded : ";
            echo $sungokong->cold_blooded;
            echo "<br>Yell : ";
            $sungokong->yell();




        ?>
    </fieldset>
</body>
</html>